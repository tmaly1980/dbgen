# README

Rails migration / scaffold generator

This little app uses VueJS to visually create (rails commands for generating) scaffolded objects (migrations, model, controller, views, and tests).

Just click on 'Add Model' and it will auto-focus on the model name input. The enter key will create a new attribute and focus automatically. Tab moves to the column type, up/down selects a new type, enter (twice) moves to the next row. 

The primary key 'id' and 'created_at' / 'updated_at' timestamp fields will automatically be created.
