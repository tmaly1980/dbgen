$(document).ready(function() {
  // var app;

  window.app = new Vue({
    el: '#app',//document.body,
    methods: {
      importingAll: function() {
        this.importExportString = null;
        this.isImportingAll = !this.isImportingAll;
      },
      exportingAll: function() {
        this.importExportString = JSON.stringify(this.databases);
        this.isExportingAll = !this.isExportingAll;
      },
      closeImportExportDialog: function() {
        this.isExportingAll = false;
        this.isImportingAll = false;
      },
      importAll: function() {
        this.load(this.importExportString);
        this.isImportingAll = false;
      },
      toggleExport: function() {
        if(!this.isExportOpen)
        {
          this.save(); // About to export.
        }
        this.isExportOpen = !this.isExportOpen;
        this.exportToRails(); // Automatic regeneration
      },
      exportToRails: function() {
        var attr, i, j, len, len1, model, ref, ref1, src;
        src = '';
        ref = this.databases[this.db].models;
        for (i = 0, len = ref.length; i < len; i++) 
        {
          // if (window.CP.shouldStopExecution(2)){break;}
          model = ref[i];
          src += "rails g scaffold " + model.name + " ";
          ref1 = model.attributes;
          for (j = 0, len1 = ref1.length; j < len1; j++) 
          {
            // if (window.CP.shouldStopExecution(1)){break;}

            attr = ref1[j];
            src += attr.name + ":" + attr.type;
            if (attr.polymorphic) {
              src += '{polymorphic}';
            }
            if (attr.index) {
              src += ':index';
            }
            if (attr.unique) {
              src += ':uniq';
            }
            if (attr.array) {
              src += ':array';
            }
            // if (attr.null) { // rails doesnt support from CLI
            //   src += ':null';
            // }
            if (attr.unsigned) {
              src += ':unsigned';
            }
            src += ' ';
          }
// window.CP.exitedLoop(1);

          src += "--skip\n";
        }
// window.CP.exitedLoop(2);

        this["exportString"] = src;
        // this.copyToClipboard();
      },
      // copyToClipboard: function() {
      //   console.log(this.$refs);
      //   console.log(this.$refs['exportString']);
      //   this.$refs['exportString'].select();
      //   document.execCommand("copy"); // copy to clipboard
      // },
      exportToJson: function() {
        this["exportString"] = JSON.stringify(this.databases[this.db].models);
        // this.copyToClipboard();
      },
      pushModel: function() {
        return this.databases[this.db].models.push({
          attributes: [
          ]
        });
      },
      pushAttribute: function(model) {
        model.attributes.push(
          {
            type: 'string'
          }
        );
      },
      removeModel: function(model) {
        var index = this.databases[this.db].models.indexOf(model);
        return this.databases[this.db].models.splice(index,1);
      },
      removeAttribute: function(model, attribute) {
        var index = model.attributes.indexOf(attribute);
        return model.attributes.splice(index,1);
      },
      databaseList: function() {
        return Object.keys(this.databases);
      },
      save: function() {
        var src;
        src = JSON.stringify(this.databases);
        console.log('Saving', Object.keys(this.databases).length);
        return window.localStorage.setItem('dbgen', src);
      },
      load: function(importString) {
        var src = importString || window.localStorage.getItem('dbgen') || {};
        console.log('Loading', src);
        this.databases = JSON.parse(src) || {};
        if(this.databases)
        {
          this.db = Object.keys(this.databases)[0];
        } else {
          this.tobbleDb = true;
        }
      },
      initModel: function(model, mix) {
        this.pushAttribute(model);
      },
      addingDatabase: function() {
        this.dbInput = null;
        return this.addingDb = !this.addingDb;
      },
      renamingDatabase: function() {
        this.dbInput = this.db;
        return this.renamingDb = !this.renamingDb;
      },
      deletingDatabase: function() {
        delete this.databases[this.db];
        var databaseList = Object.keys(this.databases);
        this.db = databaseList.length > 0 ? databaseList[0] : null;
        this.save();
      },
      setDatabase: function() {
        if(this.addingDb)
        {
          this.databases[this.dbInput] = {
            models: []
          };
          this.db = this.dbInput;
          this.dbInput = null;
          this.addingDb = false;          
        } else if (this.renamingDb) {

          this.databases[this.dbInput] = this.databases[this.db];
          delete this.databases[this.db];
          this.db = this.dbInput;

          this.dbInput = null;
          this.renamingDb = false;          
        }
        this.toggleDb = false;
        this.save();
        return true;
      }
    },
    data: {
      databases: {},
      dbName: null, 
      db: null,
      addingDb: false,
      renamingDb: false,
      isExportOpen: false,
      isImportingAll: false,
      isExportingAll: false,
      toggleDb: false,
      exportString: '',
    },
    mounted: function() {
      console.log("LOADING");
      return this.load();
    }
  });

  window.app.$watch('models', function() {
    return this.save();
  }, {deep: true});

  // Register a global custom directive called v-focus
  Vue.directive('focus', {
    // When the bound element is inserted into the DOM...
    inserted: function (el) {
      // Focus the element
      el.focus()
    }
  })

  console.log("HI");

});

